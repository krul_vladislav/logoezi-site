$(document).ready(function() {
  $(".button.e-custom-product").addClass("add-btn");
  $(".attachment-full.size-full.wp-post-image").css("width", 274);
  $(".attachment-152x167.size-152x167.wp-post-image").css(
    "margin-top",
    "-39px"
  );
  if ($(".a-tab-selected-item"))
    $("#get_search_count").text($(".a-tab-selected-item").length);
  if ($(".a-selected-item-line-wrapper"))
    $("#get_search_count").text($(".a-selected-item-line-wrapper").length);
});

function filterCategory(obj) {
  event.preventDefault();
  var id_cat = $(obj).attr("id_category");
  //console.log(id_cat);
  $.ajax({
    url: document.location.origin + "/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      id_cat: id_cat,
      _wpnonce: document.location.origin + "/wp-admin/admin-ajax.php",
      action: "filter_product_shop_page"
    },
    success: function(data) {
      $(".plate.wrapper").html(data);
      // console.log(data);
    }
  });
}

function get_color(obj) {
  var color = $(obj).attr("id");
  console.log(color);
  $("#color_input").val(color);
  console.log($("#color_input").val());
}

$(document).ready(function() {
  $("#cart-tab").click(function() {
    $("#person-tab").removeClass("active");
    $("#bookmark-tab").removeClass("active");
    $("#cart-tab").addClass("active");

    $("#person").removeClass("show");
    $("#person").removeClass("active");
    $("#bookmark").removeClass("show");
    $("#bookmark").removeClass("active");
    $("#cart").addClass("show");
    $("#cart").addClass("active");
  });
  $("#bookmark-tab").click(function() {
    $("#person-tab").removeClass("active");
    $("#cart-tab").removeClass("active");
    $("#bookmark-tab").addClass("active");

    $("#person").removeClass("show");
    $("#person").removeClass("active");
    $("#cart").removeClass("show");
    $("#cart").removeClass("active");
    $("#bookmark").addClass("show");
    $("#bookmark").addClass("active");
  });
});

// $(document).ready(function() {
//   var page_account = document.location.origin + "/account/";
//   var click_cart = $("#shop_basket");
//   click_cart.click(function(e) {
//     e.preventDefault();
//     // console.log("click");
//     // console.log(page_account);
//     window.location.replace(page_account);
//     //  load_cart_tab();
//   });
// });

function load_cart_tab() {
  //console.log(obj);
  $.ajax({
    url: document.location.origin + "/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      _wpnonce: document.location.origin + "/wp-admin/admin-ajax.php",
      action: "bild_cart_tab"
    },
    success: function(data) {
      // $(".tab-pane.fade.tab-pane-position").html(data);
      console.log(data);
    }
  });
}

function get_tab_cart() {
  //$("#person-tab").removeClass("active");
  $("#cart-tab").addClass("active");

  //$("#person").removeClass("show");
  // $("#person").removeClass("active");
  $("#cart").addClass("show");
  $("#cart").addClass("active");
}

$(document).ready(function() {
  var loginErrorDiv = document.getElementById("get_error_message_log_in");
  if (loginErrorDiv) {
    if (loginErrorDiv) {
      document.getElementById("get_error_text_log_in").style.display = "block";
    } else
      document.getElementById("get_error_text_log_in").style.display = "none";
  }
});

$(document).ready(function() {
  var get_page_regist = document.getElementById("password_1");
  if (get_page_regist) {
    document.getElementById("password_1").addEventListener("input", function() {
      get_true_pass();
    });
    document.getElementById("password_2").addEventListener("input", function() {
      get_true_pass();
    });
  }

  if (document.getElementById("show_massege_regist2")) {
    window.location.replace("http://demoproject.company/my-account/");
  }
});
function get_true_pass() {
  var pass3 = $("#password_1").val();
  var pass4 = $("#password_2").val();
  // console.log(pass3);
  if (pass4 != pass3) {
    $("#password_1").css("border", "solid 0.5px #e30613");
    $("#password_2").css("border", "solid 0.5px #e30613");
    $("#btn_registr_account").attr("disabled", "disabled");
  } else {
    $("#password_1").css("border", "solid 0.5px #efefef");
    $("#password_2").css("border", "solid 0.5px #efefef");
    $("#btn_registr_account").removeAttr("disabled");
  }
}

$(document).ready(function() {
  var page_my_account = document.location.origin + "/my-account/";
  var page_account = document.location.origin + "/account/";
  if (location.href == page_my_account) {
    $(".entry-content").addClass("login-wrapper");
    $(".woocommerce").addClass("wrapper");
    $(".woocommerce").addClass("login");
    if ($(".login-wrapper")) {
      $(".entry-title").css("display", "none");
    }
    if ($("div").is("#show_btn_lost_pass")) {
      $(".woocommerce-LostPassword.lost_password").css("display", "block");
    } else {
      $(".woocommerce-LostPassword.lost_password").css("display", "none");
    }
    $(
      ".woocommerce-button.button.woocommerce-form-login__submit.add-btn"
    ).click(function(e) {
      //e.preventDefault();
      window.location.replace(page_account);
    });
  }
});

$(document).ready(function() {
  var title = $(".screen-reader-text");
  if (title) {
    title.css("display", "none");
  }
});

function custCreateCookie(name, value, days) {
  var expires;
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
    expires = "; expires=" + date.toGMTString();
  } else {
    expires = "";
  }
  document.cookie = name + "=" + value + expires + "; path=/";
}

function filterMode(obj, mode) {
  //console.log(mode);
  if ($(obj).hasClass("cust-no-active")) {
    // console.log(mode);
    $(".list-swap-wrapper").toggleClass("active");
    custCreateCookie("prod-type-view", mode, 30);
    window.location.reload(true);
  }
}

var alertSuccess = $(".woocommerce-notices-wrapper");

alertSuccess.css("display", "none");

$(document).ready(function() {
  var paramsString = location.search;
  var searchParams = new URLSearchParams(paramsString);
  //console.log(searchParams);
  //Iterate the search parameters.
  for (let p of searchParams) {
    //    console.log(p);
  }
  //Сортировка select
  $("#sort").change(function() {
    var sortValues = $("#sort option:selected").val(); //получаем значение выбранного пункта select

    if ($("#sort").val() == "") {
      searchParams.delete("select");
      window.history.replaceState(
        {},
        "",
        location.pathname + "?" + searchParams
      );
      location.reload();
    } else {
      searchParams.set("select", sortValues);
      window.history.replaceState(
        {},
        "",
        location.pathname + "?" + searchParams
      );
      location.reload();
    }
  });
});
$("#contact_form_btn_send").click(function(event) {
  event.preventDefault();

  var val_input = $(".contact-form-email").val();

  $.ajax({
    url: document.location.origin + "/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      input_send_mess: val_input,
      _wpnonce: document.location.origin + "/wp-admin/admin-ajax.php",
      action: "send_message_at_home"
    },
    success: function(data) {
      console.log(data);
    }
  });
  var mess = $("#contact_form_btn_send");
  if (mess) {
    $(".message_popup_div").css("display", "block");
  }
  setTimeout(reload_page, 2000);
});

function reload_page() {
  location.reload();
}

$(".current-size-plus").click(function(e) {
  e.preventDefault();
  setTimeout(counterSum, 500);
});

$(".current-size-minus").click(function(e) {
  e.preventDefault();
  setTimeout(counterSum, 500);
});

$(document).ready(function() {
  var input_count = document.getElementsByClassName("current-size-quantity-2");
  for (var i = 0; i < input_count.length; i++) {
    input_count[i].addEventListener("input", function() {
      setTimeout(counterSum, 500);
    });
  }
});

function counterSum() {
  var arr = $(".current-size-quantity-2");
  var value_input_sum = 0;
  var price_prod = parseFloat($(".get_price_by_product").attr("id"));

  for (var i = 0; i < arr.length; i++) {
    if (parseFloat($(arr[i]).val())) {
      value_input_sum += parseFloat($(arr[i]).val());
      // console.log(parseFloat($(arr[i]).val()));
    }
  }
  $("#count_product_item").text(value_input_sum);
  $("#total_amount_price").text(value_input_sum * price_prod + "$");
  // plus_price(value_input_sum);
}

function plus_price(count_product) {
  var count_product = parseFloat(count_product);
  var price_product = $(".get_price_by_product").attr("id");
  total_price = count_product * price_product;
  $("#total_amount_price").text(total_price + "$");
}

function click_btn_add_cart(id_prod) {
  console.log("submit");
  $(".form_add_size_to_cart").submit();
  // $.ajax({
  //   url: "http://demoproject.company/wp-admin/admin-ajax.php",
  //   type: "post",
  //   data: {
  //     id_prod: id_prod,
  //     value_input_s: value_input_s,
  //     value_input_m: value_input_m,
  //     value_input_l: value_input_l,
  //     value_input_xl: value_input_xl,
  //     value_input_xxl: value_input_xxl,
  //     value_input_3xl: value_input_3xl,
  //     _wpnonce: "http://demoproject.company/wp-admin/admin-ajax.php",
  //     action: "add_to_cart_prod"
  //   },
  //   success: function(data) {
  //     // $('#cust_ajax_block').html(data);
  //     console.log(data);
  //     //location.reload();
  //   }
  // });
}

function filter_sub_menu(obj) {
  //console.log(obj);
  var filter_name = $(obj).attr("id");
  $.ajax({
    url: "http://demoproject.company/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      filter_name: filter_name,
      _wpnonce: "http://demoproject.company/wp-admin/admin-ajax.php",
      action: "bild_prod_view"
    },
    success: function(data) {
      $(".wrapper.plate").html(data);
      // console.log(data);
    }
  });
}

function click_to_cat(obj) {
  event.preventDefault();
  searchParams = $(obj).attr("name");
  $(".dgwt-wcas-search-input.header-search-input").val(searchParams);
  // console.log(obj);
  $(".dgwt-wcas-search-form").submit();
}

// $(".a-categorie.slick-slide.slick-current.slick-active").click(function(event) {
//   event.preventDefault();
//   searchParams = $(".a-categorie.slick-slide.slick-current.slick-active").attr(
//     "name"
//   );
//   console.log(searchParams);
//   $(".dgwt-wcas-search-input.header-search-input").val(searchParams);
//   // console.log(obj);
//   // $(".dgwt-wcas-search-form").submit();
// });
