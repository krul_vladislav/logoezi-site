<?php
/*
 *  Author: Krul
 */


/*------------------------------------*\
	Theme Support
\*------------------------------------*/

function get_color_type($id){
	global $wpdb;
	$res = $wpdb->get_results("SELECT `meta_value` FROM `wp_postmeta` WHERE `post_id`= '$id' AND `meta_key`='_product_attributes'");
	cl_print_r($res);
}

function get_sub_menu_filter($type_filter2){
if(!$type_filter2 && empty($type_filter2)){ return false; }
else{
	global $wpdb;
	$arr = array();
	$html_sub_menu = '';
	$type_filter = 'pa_'.$type_filter2;
	$res = $wpdb->get_results("SELECT `term_id` FROM `wp_term_taxonomy` WHERE `taxonomy`='$type_filter'");
		foreach($res as $val){
				$result = $wpdb->get_results("SELECT `name` FROM `wp_terms` WHERE `term_id`='$val->term_id'");
					foreach($result as $value){
						array_push($arr, $value->name);
					}
			}
			$type_filter = str_replace('pa_', '', $type_filter);
		//	cl_print_r($type_filter);
switch($type_filter){
	case 'brand':
		get_sub_menu_filter_title($type_filter); 
			echo '<div class="dropdown-menu">
							<ul class="dropdown-ul">';
					foreach($arr as $val){
						?> <li class="dropdown-item" id="<?php echo $val; ?>" onclick="filter_sub_menu(this)"> <?php echo $val; ?> </li> <?php
					} 
			echo  '</ul></div>';
	break;
		case 'gender':
			get_sub_menu_filter_title($type_filter); 
			echo '<div class="dropdown-menu">
							<ul class="dropdown-ul">';
					foreach($arr as $val){
						?> <li class="dropdown-item" id="<?php echo $val; ?>" onclick="filter_sub_menu(this)"> <?php echo $val; ?> </li> <?php
					} 
			echo  '</ul></div>';
		break;
		case 'color':
			get_sub_menu_filter_title($type_filter); 
			echo '<div class="dropdown-menu">
							<ul class="dropdown-ul">';
					foreach($arr as $val){
						?> <li class="dropdown-item" id="<?php echo $val; ?>" onclick="filter_sub_menu(this)"> <?php echo $val; ?> </li> <?php
					} 
			echo  '</ul></div>';
		break;
		case 'size':
			get_sub_menu_filter_title($type_filter); 
			echo '<div class="dropdown-menu">
							<ul class="dropdown-ul">';
					foreach($arr as $val){
						?> <li class="dropdown-item" id="<?php echo $val; ?>" onclick="filter_sub_menu(this)"> <?php echo $val; ?> </li> <?php
					} 
			echo  '</ul></div>';
		break;



		}
	}
}

function get_sub_menu_filter_title($type){
	?>
	<button
	type="button"
	class="filter_btn dropdown-toggle"
	data-toggle="dropdown"
	aria-haspopup="true"
	aria-expanded="false"><?php echo $type; ?></button>
  <?php
}

function sub_menu_filter(){
	$arr_menu = array('brand', 'gender', 'color', 'size');
	foreach($arr_menu as $value){
	?>
		<div class="btn-group">
			<?php get_sub_menu_filter($value); ?>
		</div>
	<?php
	}
}
function sub_menu_other_filter(){
	$html = '';
	$html .= '<div class="btn-group">
							<div id="js_open_more_filters" class="filter_btn dropdown-toggle">
								other filters
							</div>
						</div>';
		
	return $html;
}

function get_comment_by_prod($prod_id){
	$url_urich = get_url_for_file('img'); 
	$args = array(
		'order'               => 'DESC',
		'post_id'             => $prod_id
	);
	
	if( $comments = get_comments( $args ) ){
		foreach( $comments as $comment ){ ?>

		<div class="wrapper-comment">
          <div class="wrapper-comment-title" id="<?php echo $prod_id; ?>">
            <img class="comment-title-img" src="<?php echo $url_urich; ?>assets/user-secret.svg" alt="user's photo" />
            <div class="comment-title-head">
              <p class="comment-title-name"><?php echo $comment->comment_author; ?></p>
              <div class="selected-item-line-info-rate">
                <i class="fa fa-star gold" aria-hidden="true"></i><i class="fa fa-star gold" aria-hidden="true"></i><i
                  class="fa fa-star gold" aria-hidden="true"></i><i class="fa fa-star-half-o gold"
                  aria-hidden="true"></i><i class="fa fa-star-o bronze" aria-hidden="true"></i>
              </div>
            </div>
          </div>
          <div class="comment-text-wrapper">
            <p class="comment-text">
              <?php echo $comment->comment_content; ?>
            </p>
          </div>
		</div> 

	<?php	}
	}
}




