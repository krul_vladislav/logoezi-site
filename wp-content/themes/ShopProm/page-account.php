<?php // Template Name: Page Account?>

<?php 
 //if($fff)
if($_POST){
  cl_print_r('Данные обновились');
  $user_id = get_current_user_id();
  $user_name = $_POST['user_name'];
  $user_location = $_POST['user_location'];
  $user_last_name = $_POST['user_last_name'];
  $user_phone = $_POST['user_phone'];
  $user_email = $_POST['user_email'];
  $user_password = $_POST['password'];
  //cl_print_r($user_email);
  if($user_name){
    update_user_meta( $user_id, 'first_name', $user_name );
  }
  if($user_location){
    update_user_meta( $user_id, '_location_user', $user_location );
  }
  if($user_last_name){
    update_user_meta( $user_id, 'last_name', $user_last_name );
  }
  if($user_phone){
    update_user_meta( $user_id, '_mobile_phone', $user_phone );
  }
  if($user_email){
    wp_update_user(array( 'ID' => $user_id, 'user_email' => $user_email ) );
  }
  if($user_password){
    wp_update_user(array( 'ID' => $user_id, 'user_pass' => $user_password ) );
  }
  if(!is_user_logged_in()){
    $url_log = get_home_url(). '/log-in/';
    cl_print_r($url_log);
    wp_redirect($url_log);
  }
}


?>
<?php get_header(); ?>


<main>
      <section class="account-tabs-menu-wrapper">
        <div class="wrapper account-tabs-menu">
          <h2 class="title-h2">my account</h2>
          <div class="account-tabs">
            <div>
              <ul>
                <ul class="nav">
                <?php get_template_part('templates/account-tabs'); ?>
                 
                </ul>
              </ul>

            </div>
          <div class="tab-content" id="myTabContent">

          <!-- Personal Info -->
          <div
            class="tab-pane fade tab-pane-position show active"
            role="tabpanel"
            id="person"
            aria-labelledby="person-tab"
          >
              <?php get_template_part('templates/tabs-user-form'); ?> 
          </div>

            <div
              class="tab-pane fade tab-pane-position"
              role="tabpanel"
              id="cart"
              aria-labelledby="cart-tab">
                <?php get_template_part('templates/tabs-user-cart'); ?> 
            </div>
               <div
                class="tab-pane fade tab-pane-position"
                role="tabpanel"
                id="bookmark"
                aria-labelledby="bookmark-tab"
              >
              <section class="wrapper-plate">

                <div class="plate wrapper">

                <?php $best_sell_products_query = query_posts([
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'posts_per_page' => '10000',
                    'orderby' => 'post_modified'
                  ]);

                        if ($best_sell_products_query): ?>
                                    <?php echo do_action('ProductBookmarks',$best_sell_products_query); ?>
                        <?php endif; ?>
                
                </div>
                <div class="showmore"><div id="showmore-button" class="showmore-btn" style="display: none">show more</div></div>

                </section>
              </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <div class="woocommerce-MyAccount-content">
	<?php
		/**
		 * My Account content.
		 *
		 * @since 2.6.0
		 */
	//	do_action( 'woocommerce_account_content' );
	?>
</div>
    </main>
   

<?php get_footer(); ?>