<?php // Template Name: Page Service?>

<?php get_header(); ?>
<main>
      <section class="services-wrapper">
        <div class="services wrapper">
          <h2 class="title-h2">services</h2>
        </div>
      </section>
     
      <section class="service-content-wrapper">
        <div class="wrapper service-content">
           <?php if ( have_posts() ) : 
               query_posts('cat=41'); 
                  while (have_posts()) : the_post(); 
                   $text = get_the_content();
                    preg_match("|<p>(.*)</p>|is", $text, $result);
                  ?> 
          <div class="about_name">
            <div class="about_name_img_wrapper">
            <?php echo get_the_post_thumbnail( get_the_ID(), array(364, 247)); ?>
            </div>
            <div class="about_name_p_wrapper">
              <p class="about_title"><?php the_title();   ?></p>
              <p class="about_name_p">
              <?php echo $result[1]; ?>
           </p>
            </div>
          </div>
                  <?php     endwhile; 
              endif; 
            wp_reset_query(); ?>

        </div>
      </section>
    </main>

<?php get_footer(); ?>