<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @package proficiency
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="UTF-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous"
    />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/inc/Urich/ready-css/all.min.css" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/inc/Urich/font-awesome/css/font-awesome.min.css" />
    <title>Promotional Clothing</title>
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri() ?>/inc/Urich/assets/photo-3.png" type="image/x-icon" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
<header>
<div class="header wrapper">
  	
        <?php $phone = get_phone_header();
              $schedule = get_schedule_header(); 
              $image = get_image_header(); 
              ?>
        <a href="<?php echo get_home_url(); ?>"
          ><div class="logotype">
            <img src="/wp-content/themes/ShopProm/inc/Urich/assets/logotype.png" alt="logotype" />
          </div
        ></a>
        <div class="navigation">
          <div id="js_header_search_input" class="header-search-input-wrapper">
            <i class="fa fa-search" aria-hidden="true"></i>
           <?php echo do_shortcode('[wcas-search-form]'); ?>
          </div>
          <div class="navigation-icons">
            <i id="js_search_btn" class="fa fa-search" aria-hidden="true"></i>
           <?php if(is_user_logged_in()) { ?>
            <a href="<?php echo get_home_url(); ?>/account">
           <?php } else { ?>
            <a href="<?php echo get_home_url(); ?>/log-in">
            <?php } ?>
              <i class="fa fa-user" aria-hidden="true"></i></a>
              <?php if(is_user_logged_in()) { ?>
            <a href="<?php echo get_home_url(); ?>/cart-2">
            <?php } else { ?>
            <a href="<?php echo get_home_url(); ?>/log-in">
            <?php } ?>
              <i id="shop_basket" class="fa fa-shopping-cart" aria-hidden="true">
                <div><?php echo WC()->cart->get_cart_contents_count();?></div></i> </a>
                <?php if(is_user_logged_in()) { ?>
              <a href="<?php echo wp_logout_url(get_home_url()); ?>">
                <i class="fa fa-sign-out" aria-hidden="true"></i></a>
                <?php } else { ?>
            <?php } ?>
          
          </div>
          <div class="navigation-info">
            <span class="navigation-info-working-time"><?php echo $schedule[0]; ?></span
            ><a class="navigation-info-mobile" href="tel:<?php echo $phone[0]; ?>"><?php echo $phone[0]; ?></a>
          </div>
        </div>
        
      </div>
    </header>

    <?php 
