<?php // Template Name: Page Home front?>

<?php
// if($_POST['account_email'] ){
//  $admin_mail = 'gavrick1910@gmail.com';
//   $user_mess = $_POST['account_email'];
//   send_message_at_home_page($admin_mail, $user_mess);
//   echo '<div id="message_popup_div_id55">'.$admin_mail.'</div>';
// }
?>


<?php get_header(); ?>

<?php $url_urich = get_url_for_file('img'); ?>
<main>
  <section class="section-baner">
		<div id="baner_total" class="section-baner-wrapper">
      <?php  echo do_action('MainBanner'); ?>
    </div>
    <div class="baner-counter">
				<?php echo do_action('Paginator_main'); ?>
				</div>
	</section>

  <section class="section-tab-spots">
    <div class="tab-spots wrapper">
      <div class="tab-spots-list">
            <ul>
              <ul class="nav">
                <li class="nav-item">
                  <a
                    data-toggle="tab"
                    role="tab"
                    aria-selected="true"
                    class="nav-link tab-spot active nav-item-link"
                    id="topsellers-tab"
                    href="#topsellers"
                    aria-controls="topsellers"
                    ><img class="nav-item-a-img" src="<?php echo $url_urich; ?>assets/loving-heart-shape.svg" alt="heart" />Topsellers</a
                  >
                </li>
                <li class="nav-item">
                  <a
                    data-toggle="tab"
                    role="tab"
                    aria-selected="false"
                    class="nav-link tab-spot nav-item-link"
                    id="new-items-tab"
                    href="#new-items"
                    aria-controls="new-items"
                    ><img class="nav-item-a-img" src="<?php echo $url_urich; ?>assets/store-new-badges.svg" alt="heart" />New items</a
                  >
                </li>
              </ul>
            </ul>
          </div>
          <div class="tab-content" id="myTabContent">
            <div
              class="tab-pane fade show active tab-pane-position"
              role="tabpanel"
              id="topsellers"
              aria-labelledby="topsellers-tab"
            >

      <div id="tab-selected-js-first" class="tab-selected tab-selected-js">
                <?php $best_sell_products_query = query_posts([
                  'post_type' => 'product',
                  'post_status' => 'publish',
                  'product_cat' => 'topsellers'
                ]);
        if ($best_sell_products_query): ?>
                    <?php echo do_action('ProductSlider',$best_sell_products_query); ?>
        <?php endif; ?>
      </div>
        
              <div id="js_tab_counter_topsellers" class="tab_page_counter">
                <?php echo do_action('Paginator_main'); ?>
              </div>
    </div>
            <div class="tab-pane fade tab-pane-position" role="tabpanel" id="new-items" aria-labelledby="new-items-tab">
              <div id="tab-selected-js-second" class="tab-selected tab-selected-js">

                <?php $best_sell_products_query = query_posts([
                    'post_type' => 'product',
                    'post_status' => 'publish',
                     'posts_per_page' => '30',
                    'orderby' => 'post_modified'
                  ]);
                if ($best_sell_products_query): ?>
                            <?php echo do_action('ProductSlider',$best_sell_products_query); ?>
                <?php endif; ?>
              </div>
            <div id="js_tab_counter_newitems" class="tab_page_counter">
              <?php echo do_action('Paginator_main'); ?>
            </div>
          </div>
        </div>
      </div>
    </section>

      <section class="section-promo">
        <div class="promo wrapper">
          <div class="promo-block-small-wrapper">

          <!-- first block -->
          <?php echo do_action('section_promo','p=159', 'red' ); ?>
            <!-- 2 block -->
            <?php echo do_action('section_promo','p=165', 'blue' ); ?>
            <!-- 3 block -->
              <?php echo do_action('section_promo','p=167', 'yellow' ); ?>  
          </div>
            <!-- 4 block -->
          <?php echo do_action('section_promo','p=194','false'); ?>  

        </div>
      </section>
      <section class="section-discount">
        <div class="discount wrapper">
          <div class="discounted tab-pane-position">
            <h2 class="title-h2">discounted items</h2>
            <div id="tab-selected-js-discount" class="tab-selected">
           
              <?php $best_sell_products_query = query_posts([
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'posts_per_page' => '30',
                    'product_cat' => 'discount'
                  ]);
              if ($best_sell_products_query): ?>
                          <?php echo do_action('ProductSlider',$best_sell_products_query); ?>
              <?php endif; ?>

            </div>

            <div id="js_discounted" class="tab_page_counter">
              <?php echo do_action('Paginator_main'); ?>
            </div>
          </div>
          <div class="recommendations tab-pane-position">
            <h2 class="title-h2">recommendations</h2>
      
            <div id="tab-selected-js-recomendations" class="tab-selected">
           
              <?php $best_sell_products_query = query_posts([
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    'ignore_sticky_posts' => 1,
                    'posts_per_page' => '10',
                    'columns' => '6',
                    'meta_key' => 'total_sales',
                    'orderby' => 'meta_value_num',
                    'meta_query' => WC()->query->get_meta_query(),
                    'product_cat' => 'Recommendations'
                  ]);
                  if ($best_sell_products_query): ?>
                              <?php echo do_action('ProductSlider',$best_sell_products_query); ?>
                  <?php endif; ?>
            </div>

            <div id="js_recommendations" class="tab_page_counter">
              <?php echo do_action('Paginator_main'); ?>
            </div>
          </div>
        </div>
      </section>

      <section class="section-brands">
        <div class="brands wrapper">
          <h2 class="title-h2">our brands</h2>
          <div id="our_brands_js" class="tab-selected brands-slots">

            <?php echo do_action('Add_Brands_main'); ?>
           
          </div>
          <div id="js_brands" class="tab_page_counter">
             <?php echo do_action('Paginator_main'); ?>
          </div>
        </div>
      </section>

      <section class="section-categories">
        <div class="categories wrapper">
            <h2 class="title-h2">categories</h2>
            <div class="categories_wrapper">
              <?php get_template_part('templates/category-view'); ?>
          </div>
        </div>
      </section>


     <?php get_template_part('templates/about-us'); ?>

      <section class="section-contacts">
        <div class="contacts wrapper">
          <div class="contact-form">
            <h3 class="contact-form-title">Subscribe Logoezi Newsletter</h3>
            <p class="contact-form-subtitle">Find out about sales and new items the first</p>
            <form id="form-submit" action="" method="post" class="contact-form-wrapper">
              <input class="contact-form-email" type="email" name="account_email" id="account_email" placeholder="Enter your e-mail" />
              <button class="contact-form-btn" id="contact_form_btn_send">Subscribe</button>
              <div class="message_popup_div"><p class="message_popup">Your message has successfully sent. Thank you</p></div>
            </form>
          </div>
          <div class="contacts-form-img-wrapper">
            <img class="contacts-form-img" src="<?php echo $url_urich; ?>assets/couple-form.png" alt="image-form" />
          </div>
        </div>
      </section>
    </main>
    
	<!-- /section -->

<?php get_footer(); ?>