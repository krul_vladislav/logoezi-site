<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$product_id = get_the_ID();
$url_urich = get_url_for_file('img'); 

//$product = wc_get_product($product_id );
$link = get_permalink($product_id );
$url_urich = get_url_for_file('img'); 
$product_name = $product->get_name();
$price = $product->get_regular_price() . ' ' .'$';
if (has_post_thumbnail()) $product_image = get_the_post_thumbnail_url($product_id , 'medium');
$color = $product->get_attribute('pa_color');
$color = explode(', ', $color);
            $size = $product->get_attribute('pa_size');   
            $size = explode(', ', $size);
            $product_image_gallery = $product->get_gallery_image_ids( 'view' );

            $attachments         = array_filter( $product_image_gallery );
            $update_meta         = false;
            $updated_gallery_ids = array();
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
  <!-- <main> -->
		<section class="destiny-wrapper">
      		<div class="destiny">
				<?php echo do_action('breadcrumb'); ?>
			</div>
		</section>

	<section class="wrapper-item-card" style="height:675px">
      <div class="item-card">
        <div class="item-card-title">
			<div class="item-card-title">
			<p class="item-card-title-text"><?php echo $product_name; ?></p>
			</div>
			<div class="selected-item-line-info-rate">
            <i class="fa fa-star gold" aria-hidden="true"></i><i class="fa fa-star gold" aria-hidden="true"></i
            ><i class="fa fa-star gold" aria-hidden="true"></i><i class="fa fa-star-half-o gold" aria-hidden="true"></i
            ><i class="fa fa-star-o bronze" aria-hidden="true"></i>
          </div>
			<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
<!-- ыыыыыыыыыыыыыыыыыыы -->
	<?php do_action( 'woocommerce_before_single_product_summary' ); ?>
	<!-- оооооооооооооооооооо -->
		<div class="summary entry-summary">
	
	<?php	do_action( 'woocommerce_single_product_summary' ); ?>
				</div>
	  	      <input type="hidden" id="<?php echo $product->get_regular_price(); ?>" class="get_price_by_product">

	<?php do_action( 'woocommerce_after_single_product_summary' ); ?>
	
		</div>
		  </div>
</div>
 
<?php do_action( 'woocommerce_after_single_product' ); ?>

</section>
		<!-- </main> -->
<!--  -->	
<section  class="wrapper-review">
      <h2 class="title-h2">reviews</h2>
     	 <div class="review-slider">
			<?php get_comment_by_prod(the_ID()) ?>
		 </div>
				<div id="js_review_slider" class="tab_page_counter">
					<?php echo get_pagination_page(); ?>
				</div>
      <div class="add-btn" id="add_review">add a review</div>
	</section>

		<section class="wrapper-suggestion">
     	<div class="suggestion">
        	<h2 class="title-h2">customers also bought</h2>
        		<div id="customers_also_bought" class="tab-selected">

				<?php 
			//	echo do_shortcode('[related_products per_page="30"]');
				$best_sell_products_query = query_posts([
                    'post_type' => 'product',
                    'post_status' => 'publish',
                    // 'ignore_sticky_posts' => 1,
                     'posts_per_page' => '1000',
                    // 'columns' => '3',
                    // 'meta_key' => 'total_sales',
                    'orderby' => 'post_modified'
                    // 'meta_query' => WC()->query->get_meta_query(),
                     //'product_tag' => '-25%'
                  ]);
                if ($best_sell_products_query): ?>
														 <?php 
														 echo do_action('ProductSlider',$best_sell_products_query); ?>
				 				<?php 
				 				endif; 
								?>
				</div>
			<div id="js_customers_also_bought" class="tab_page_counter">
				<?php echo get_pagination_page(); ?>
			</div>
        </div>
		</section>