$(document).ready(function() {
  $(".bookmark-ico-container").click(function(event) {
    event.preventDefault();
    var bool_add = false;
    if (!$(this).hasClass("bookmark-active")) {
      $(this).addClass("bookmark-active");
      bool_add = true;
    } else {
      $(this).removeClass("bookmark-active");
    }
    var prod_id = $(this).attr("id");
    ajax_add_marks(prod_id, bool_add);
  });
});

function ajax_add_marks(id, has) {
  $.ajax({
    url: window.location.origin + "/wp-admin/admin-ajax.php",
    type: "post",
    data: {
      product_id: id,
      state: has,
      _wpnonce: window.location.origin + "/wp-admin/admin-ajax.php",
      action: "get_state_mark"
    },
    success: function(data) {
      // $(".wrapper.plate").html(data);
      console.log(data);
    }
  });
}

$(document).ready(function() {});

// Выбор цвета на странице товара
function get_color(element) {
  $.each(
    $(element)
      .closest(".color-wrapper-line")
      .find(".color"),
    function(index, item) {
      $(item).css("transform", "scale(1)");
    }
  );
  $(element).css("transform", "scale(1.5)");
}

// Добавление стилей в блок добоавления комментария
$(document).ready(function() {
  $(".single-product main").css("padding", "0");
  $(".comments-title").addClass("title-h2");
  $("#commentform .form-submit").addClass("form-flex-center");
  $("#submit").addClass("add-btn");
  $("#submit").val("Leave a review");
});

// Кнопка открытия меню добавления комментария
$("#add_review").click(function() {
  $("#comments").css("display", "block");
  var target = $("#comments");
  $("html, body").animate({ scrollTop: $(target).offset().top }, 1000);
  return false;
});

// Кнопка "оставить комментарий"
$("#submit").click(function() {
  $("#comments").css("display", "none");
});

$(document).ready(function() {
  $(".entry-header").removeClass("entry-header");
  $(".entry-title")
    .removeClass("entry-title")
    .addClass("title-h2");

  $("header h1").css("display", "none");
});

// baner: slick-slider
$(document).ready(function() {
  var viewportWidth = $("body").innerWidth();
  if (viewportWidth <= 600) {
    setTimeout(function() {
      $(".section-baner .slick-arrow").css("display", "none");
      $(".section-baner .slick-dots").css("display", "none");
    }, 0);
  }
});
