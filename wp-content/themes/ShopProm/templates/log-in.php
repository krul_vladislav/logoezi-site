
<?php // Template Name: Page Log-in?>
<?php
if($_POST){
  cl_print_r($_POST);
 $user_login = $_POST['user_regist_email'];
 $user_pass = $_POST['user_regist_pass'];

if($user_login != ''){
  $creds = array();
  $creds['user_login'] = $user_login;
  $creds['user_password'] = $user_pass;
  $creds['remember'] = true;

  $user = wp_signon( $creds, false );

    if ( is_wp_error($user) ) {
      cl_print_r('error');
      echo '<div id="get_error_message_log_in"></div>';
    }
    else{
      wp_redirect(get_home_url());
    }
  }
}

?>

<?php get_header(); ?>
   <main>
   <section class="login-wrapper">
     <div class="wrapper login">
        <form action="" method='post'>
          <div class="login-popup-wrapper">
            <p class="login-popup-title">Log In</p>
            <input class="login-popup-input" name="user_regist_email" type="email" placeholder="Enter your e-mail" />
            <input class="login-popup-input" name="user_regist_pass" type="password" placeholder="Enter your password" />
            <button href="accountPage.html" class="add-btn">log in</button>

            <p class="login-popup-advice" id="get_error_text_log_in" style="margin-top:-35px; display:none">
              The password you entered for address <?php echo $user_login; ?> is incorrect.  
              <a class="login-popup-advice-link" href="<?php echo get_home_url(); ?>/account/">Forgot your password?</a>
            </p>
            <p class="login-popup-advice">
              If you don't have an account, then
              <a class="login-popup-advice-link" href="<?php echo get_home_url(); ?>/registration/">register</a>
            </p>
          </div>
        </form>
       <div class="login-screen-img-wrapper">
         <img class="login-screen-img" src="<?php echo get_template_directory_uri() ?>/inc/Urich/assets/couple-login.png" alt="login" />
       </div>
     </div>
   </section>
 </main>

 <?php get_footer(); ?>
