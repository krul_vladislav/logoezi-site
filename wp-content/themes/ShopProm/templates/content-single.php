<?php

global $product;
$product_id = get_the_ID();
defined( 'ABSPATH' ) || exit;

if($_POST){
  cl_print_r($_POST);
  //  do_action( 'woocommerce_add_cart_item_data2', $_POST,$product_id);
   // do_action( 'woocommerce_add_cart_item_data', $_POST,$product_id);
}


$url_urich = get_url_for_file('img'); 

//$product = wc_get_product($product_id );
$link = get_permalink($product_id );
$url_urich = get_url_for_file('img'); 
$product_name = $product->get_name();
$price = $product->get_regular_price() . ' ' .'$';
if (has_post_thumbnail()) $product_image = get_the_post_thumbnail_url($product_id , 'medium');
$color = $product->get_attribute('pa_color');
$color = explode(', ', $color);
            $size = $product->get_attribute('pa_size');   
            $size = explode(', ', $size);
            $product_image_gallery = $product->get_gallery_image_ids( 'view' );

            $attachments         = array_filter( $product_image_gallery );
            $update_meta         = false;
            $updated_gallery_ids = array();
            //cl_print_r($price);
?>
<section class="wrapper-item-card">
      <div class="item-card wrapper">
        <div class="item-card-title">
          <p class="item-card-title-text"><?php echo $product_name; ?></p>
        </div>
        <div class="selected-item-line-info-rate">
          <i class="fa fa-star gold" aria-hidden="true"></i><i class="fa fa-star gold" aria-hidden="true"></i><i
            class="fa fa-star gold" aria-hidden="true"></i><i class="fa fa-star-half-o gold" aria-hidden="true"></i><i
            class="fa fa-star-o bronze" aria-hidden="true"></i>
        </div>
        <div class="wrapper-item-card-info">
          <div class="wrapper-slider-photo">
            <div class="slider-for">
            <div class="slider-for-img-wrapper"><img class="img-size" src="<?php echo $product_image; ?>" alt="" /></div> 
            <?php
                if ( ! empty( $attachments ) ) {
                            foreach ( $attachments as $attachment_id ) {
                                $attachment = wp_get_attachment_image( $attachment_id, 'thumbnail' );
                                   // echo count($attachments);
                                    if(count($attachments)>1){
                                // if attachment is empty skip.
                                if ( empty( $attachment ) ) {
                                    $update_meta = true;
                                    continue;
                                }
        
                                echo '<div class="slider-for-img-wrapper" data-attachment_id="' . esc_attr( $attachment_id ) . '">
                                        ' . $attachment . '
                                    </div>';
        
                                // rebuild ids to be saved.
                                $updated_gallery_ids[] = $attachment_id;
                            }
                            }
                        }
                ?>
               
             </div>

            <div class="slider-nav">

            <?php
            
                if ( ! empty( $attachments ) ) {
                            foreach ( $attachments as $attachment_id ) {
                                 $attachment = wp_get_attachment_image( $attachment_id, array('88', '88') );
                                
        //cl_print_r($index);
                                // if attachment is empty skip.
                                if ( empty( $attachment ) ) {
                                    $update_meta = true;
                                    continue;
                                }
        
                                echo '<div class="slider-for-img-wrapper-little" data-attachment_id="' . esc_attr( $attachment_id ) . '">
                                        ' . $attachment . '
                                    </div>';
        
                                // rebuild ids to be saved.
                                $updated_gallery_ids[] = $attachment_id;
                            }
                        }
                ?>

            </div>
          </div>
          <div class="wrapper-properties">
            <p class="properties-title">Choose your properties and quantity</p>
            <div class="size-color-wrapper">
              <div class="selected-item-line-info-color-wrapper">
                <p class="color-wrapper-color">Color:</p>
                <div class="color-wrapper-line">
                <?php 
                    if($color ){
                       // cl_print_r($color);
                       foreach($color as $key=>$val){
                         if($val){
                           if($key<10) ?>
                           <div class="color" id="<?php echo $val ?>" style="background: <?php echo $val ?>" onclick="get_color(this)"></div>
                        <?php }
                      }
                    }
                  ?>
                </div>
              </div>

              <div class="chosen-size-wrapper">
                <p class="chosen-size-text">size:</p>
                <form action="" method="post" class="form_add_size_to_cart">
                <div class="size-diversity flex-410">
                
                <?php 
                    if($size ){
                       // cl_print_r($size);
                       foreach($size as $key=>$val){
                         if($val){
                           if($key<10){ ?>
                           <div class="current-size">
                                  <p class="current-size-mark"><?php echo $val ?></p>
                                  <button class="btn-size-controller current-size-plus">+</button>
                                  <input class="current-size-quantity-2 xs" id="<?php echo $val ?>" type="text" name="size-<?php echo $val ?>" value="0" />
                                  <input type="hidden" id="color_input" name="color[]" value="">
                                  <button class="btn-size-controller current-size-minus">-</button>
                                  </div>
                         <?php  }
                         }
                      }
                    }
                  ?>
                  
                </div>
                    <button id="add_size_to_cart" style="opacity: 0;"></button>
                </form>
              </div>
            </div>
            <div class="total-price-wrapper">
              <p class="total-price-text">total cost</p>
              <div class="total-amount">
                <p class="total-items-sum" id="count_product_item">0</p> 
                  <span>*</span> 
                  <?php echo $price; ?>
                    <?php //do_action( 'woocommerce_single_product_summary_price' ); ?>
                <p class="total-amount" style="    margin-left: 10px;" id="total_amount_price"> 0.00$</p>
              </div>
            </div>
            <div class="customize-cart-btns">
              <a href="#" class="add-btn">customize</a>
              <!-- <div class="add-btn" id="click_btn_add_to_cart">add to cart</div> -->
              <div class="add-btn"  onclick="click_btn_add_cart(<?php echo $product_id; ?>)">add to cart</div>
              <?php //do_action( 'woocommerce_single_product_summary' );     //   Добавить в корзину ?>
            </div>
          </div>
        </div>
      </div>
      <input type="hidden" id="<?php echo $product->get_regular_price(); ?>" class="get_price_by_product">
    </section>





<style>
.price{
    font-family: "Montserrat", sans-serif !important;
    font-size: 12px !important;
    font-weight: 300 !important;
    font-style: normal !important;
}
</style>
   
