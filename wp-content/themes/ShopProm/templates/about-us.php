
<?php $url_urich = get_url_for_file('img'); ?>
<section class="section-about">
<div class="about wrapper">
  <h2 class="title-h2">about us</h2>
  <div class="about_name">
    <div class="about_name_img_wrapper">
      <img class="about_name_img" src="<?php echo $url_urich; ?>assets/hospitality_nano_1.png" alt="picture" />
    </div>
    <div class="about_name_p_wrapper">
      <p class="about_name_p">
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
        labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
        ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
        dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
        magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
        clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
      </p>
    </div>
  </div>
  <div class="about_name">
    <div class="about_name_img_wrapper">
      <img class="about_name_img" src="<?php echo $url_urich; ?>assets/nano_1.png" alt="picture" />
    </div>
    <div class="about_name_p_wrapper">
      <p class="about_name_p">
        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut
        labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et
        ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum
        dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore
        magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
        clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
      </p>
    </div>
  </div>
</div>
</section>


