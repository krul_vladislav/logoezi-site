<?php 

?>

<?php $url_urich = get_url_for_file('img'); ?>

                  <li class="nav-item">
                    <a
                      data-toggle="tab"
                      role="tab"
                      aria-selected="true"
                      class="nav-link tab-spot active nav-item-link"
                      id="person-tab"
                      href="#person"
                      aria-controls="person"
                      ><img class="nav-item-a-img" src="<?php echo $url_urich; ?>assets/user-icon.svg" alt="heart" />Personal Info</a
                    >
                  </li>
                  <li class="nav-item">
                    <a
                      data-toggle="tab"
                      role="tab"
                      aria-selected="false"
                      class="nav-link tab-spot nav-item-link"
                      id="cart-tab"
                      href="#cart"
                      aria-controls="cart"
                      ><img class="nav-item-a-img" src="<?php echo $url_urich; ?>assets/cart-icon.svg" alt="heart" />Cart</a
                    >
                  </li>
                  <li class="nav-item">
                    <a
                      data-toggle="tab"
                      role="tab"
                      aria-selected="false"
                      class="nav-link tab-spot nav-item-link"
                      id="bookmark-tab"
                      href="#bookmark"
                      aria-controls="bookmark"
                      ><img class="nav-item-a-img" src="<?php echo $url_urich; ?>assets/bookmarks.svg" alt="heart" />Bookmarks</a
                    >
                  </li>


