<?php // Template Name: Page Registration?>

<?php
if($_POST){
   //cl_print_r($_POST);
 if(!get_user_by_email ($_POST['account_email'])){
  $user_id = add_info_usermeta();
 
    if($user_id){
      echo '<p class="register-title" id="show_massege_regist2">Your account has been created</p>';
    }
  }
}
?>


<?php get_header(); ?>

<main>
  <?php if(is_user_logged_in()){
   echo '<p class="register-title" id="show_massege_regist2">Your account has been created</p>';
  } ?>
      <section class="register-wrapper">

          <div class="wrapper register">
            <p class="register-title">Set up a user account</p>
                   <form id="form-submit" action="" method="post" class="register-form-container"> 
            <div class="register-form-wrapper">

              <div class="register-form-column">
                <p class="register-form-title">personal info</p>
                <input class="register-form-input" type="text" name="account_first_name" id="account_first_name" required placeholder="Enter your first name" />
                <input class="register-form-input" type="text" name="account_last_name" id="account_last_name" required placeholder="Enter your last name" />
                <input class="register-form-input" type="email" name="account_email" id="account_email" required placeholder="Enter your e-mail" />
              </div>

              <div class="register-form-column">
                <p class="register-form-title">create a password</p>
                <input class="register-form-input" id="password_1" minlength="8" type="password" name="password" required placeholder="Enter your password" />
                <input class="register-form-input" id="password_2" minlength="8" type="password" required placeholder="Repeat your password" />

                <div class="register-checkbox">
                  <label>
                    <input class="cb pristine" type="checkbox" name="subcribe_newsletter"/>
                    <span class="register-checkbox-text">Subcribe to the newsletter</span>
                  </label>
                </div>
              </div>
                
            </div>
               <button class="add-btn" id="btn_registr_account" >register</button>
            </form>
            <p class="register-title" id="show_massege_regist" style="display:none">Your account has been created</p>
          </div>

    
      </section>
    </main>

   

<?php get_footer(); ?>