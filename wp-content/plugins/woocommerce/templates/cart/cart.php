<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;
$count_prod = 1;

do_action( 'woocommerce_before_cart' ); ?>
<form class="woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>

	<div class="shop_table shop_table_responsive cart woocommerce-cart-form__contents" cellspacing="0">
      <div class="cart-header">
        <div class="position-name-wrapper">
          <p class="position-name number">№</p>
          <p class="position-name">Item Name</p>
        </div>
        <div class="position-name-wrapper">
          <p class="attr-name">Color</p>
          <p class="attr-name">Size</p>
          <p class="attr-name">Number</p>
          <p class="attr-name">Cost</p>
        </div>
      </div>
    </div>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>
      <div class="items-cart-list">
			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
       
        // $_product   = apply_filters( 'woocommerce_cart_item_product', 
        //   $cart_item['data'], $cart_item, $cart_item_key );

        $_product   = apply_filters( 'woocommerce_get_item_data', 
        $cart_item['data'], $cart_item);

        //$result = apply_filters( 'woocommerce_add_cart_item_data', $cart_item['data'], $cart_item);
        $result2 = apply_filters('woocommerce_get_cart_item_from_session', $cart_item, $_product ,$cart_item_key);
        //cl_print_r($result2);
       // cl_print_r($_product);
        $product_id = apply_filters( 'woocommerce_cart_item_product_id', 
          $cart_item['product_id'], $cart_item, $cart_item_key );

          $product_image = get_the_post_thumbnail_url($product_id , 'medium');

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
          ?>
          
					<div class="woocommerce-cart-form__cart-item item-cart <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                       
          <!-- first block -->
          <div class="item-cart-title">
                        <!-- index product -->
            <p class="item-cart-title-text text-number"><?php echo $count_prod; ?></p>
                            <!-- image product -->
						<div class="product-thumbnail item-cart-title-img-wrapper">
              <img class="item-cart-title-img" src="<?php echo $product_image; ?>"/>
						</div>
                                <!-- title productmmmmmmmm -->
            <!-- <p class="item-cart-title-text text-description"><?php echo $_product->get_name(); ?></p> -->
            <?php
						if ( ! $product_permalink ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
						} else {
             echo '<div></div>';
              echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
						}
            do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );
            
            echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.
            
						if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
							echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
						}
						?>
          </div>


          <!-- second block -->
            <div class="item-cart-attributes">
                     <!-- product price -->
                <?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok. ?>
                        <!-- count prod -->
        <div class="current-size">
          <?php
							$product_quantity = woocommerce_quantity_input( array(
                'classes'  => "current-size-quantity",
                'input_id' => "item-pr-$count_prod",
								'input_name'   => "cart[{$cart_item_key}][qty]",
								'input_value'  => $cart_item['quantity'],
								'product_name' => $_product->get_name(),
							), $_product, false );
						

						echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
          ?>
        </div>
                      <!-- total price -->
        <p class="item-cart-price">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
            </p>
                    <!-- remove product -->
            <div class="product-remove">
              <?php
                // @codingStandardsIgnoreLine
                echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                  '<a href="%s" class="remove fa fa-trash a-trash" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
                  esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                  __( 'Remove this item', 'woocommerce' ),
                  esc_attr( $product_id ),
                  esc_attr( $_product->get_sku() )
                ), $cart_item_key );
              ?>
            </div>
          </div>
          </div>
					<?php
        }
        $count_prod++;
			}
			?>
 </div>
			<?php do_action( 'woocommerce_cart_contents' ); ?>

			<div>
				<div colspan="6" class="actions">

					<button style="height:1px; opacity:0" type="submit" class="button update_my_cart" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

					<?php do_action( 'woocommerce_cart_actions' ); ?>

					<?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
				</div>
			</div>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
     
	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<div class="cart-collaterals">
	<?php
		/**
		 * Cart collaterals hook.
		 *
		 * @hooked woocommerce_cross_sell_display
		 * @hooked woocommerce_cart_totals - 10
		 */
		do_action( 'woocommerce_cart_collaterals' );
	?>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
